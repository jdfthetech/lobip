# lobip
Life of Brian Image Processor.  Create thumbnails / smaller images of a directory of jpgs or pngs.

Written in python using pillow and tkinter.

Will require pillow to be installed.

info here:

https://pillow.readthedocs.io/en/stable/install